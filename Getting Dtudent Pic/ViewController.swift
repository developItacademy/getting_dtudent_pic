//
//  ViewController.swift
//  Getting Dtudent Pic
//
//  Created by Steven Hertz on 10/31/21.
//

import UIKit


enum URLError: Error {
    case unexpectedError
}

class ViewController: UIViewController {
    
    // used to execute the dataRetrevial from the Web API
    var webApiJsonDecoder = WebApiJsonDecoder()
    
    
    @IBOutlet weak var theStudentPic: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Access the unaum to build the urlRequest and URLSession
        
        // 1 - Get the list of classes
        let urlValues = URLValues.urlForListOfClasses
        webApiJsonDecoder.getTheClassFromWeb(with: urlValues.getUrlRequest(), andSession: urlValues.getSession() ) {(data) in
            // OK we are fine, we got data - so lets write it to a file so we can retieve it
            let aClassesReturnObject: ClassesReturnObjct = self.webApiJsonDecoder.processTheData(with: data) { print("**** From completion handler") }
            self.webApiJsonDecoder.theClassesReturnObjct = aClassesReturnObject
            
            dump(self.webApiJsonDecoder.theClassesReturnObjct)
            
            guard let classes: [ClassesReturnObjct.Classe] = (self.webApiJsonDecoder.theClassesReturnObjct?.classes),
                  let idx = classes.firstIndex(where: { $0.userGroupId == 18} )
            else {fatalError("couldn't find the student")}
            
            let classuuid = classes[idx].uuid
            print(classuuid)
            
            
            // 2 - Get the students in a class
            let urlValuesforClass = URLValues.urlForClassInfo(UUISString: classuuid)
            self.webApiJsonDecoder.getTheClassFromWeb(with: urlValuesforClass.getUrlRequest(), andSession: urlValuesforClass.getSession() ) {(data) in
                // OK we are fine, we got data - so lets write it to a file so we can retieve it
                let aClassReturnObjct: ClassReturnObjct = self.webApiJsonDecoder.processTheData(with: data) { print("**** From completion handler") }
                self.webApiJsonDecoder.theClassReturnObjct = aClassReturnObjct
                dump(self.webApiJsonDecoder.theClassReturnObjct)
                
                let student = self.webApiJsonDecoder.theClassReturnObjct?.class.students[2]
                dump(student)
                
                
                // 3 - Get a students picture
                guard let studentPhotoUrl = student?.photo.absoluteString else {fatalError("could not convert the url to string")}
                let urlValuesforStudent = URLValues.urlForStudentPic(picUrlString: studentPhotoUrl)
                self.webApiJsonDecoder.getTheClassFromWeb(with: urlValuesforStudent.getUrlRequest(), andSession: urlValuesforStudent.getSession() ) {(data) in
                    print(data)
                    let studentPic = UIImage(data: data)
                    DispatchQueue.main.async {
                        self.theStudentPic.image = studentPic
                    }
                }
                
            }
            
        }
        
    }
    
}

